<?php

namespace tor;

class exitnodes extends core
{

    /**
     * Download URL for TOR Exit Nodes
     *
     * @var string
     */
    public $downloadURL = 'https://check.torproject.org/cgi-bin/TorBulkExitList.py?ip=8.8.8.8';

    /**
    * Download Preformatted List.
    * 
    * @var string
    */
    public $formattedList = 'https://raw.githubusercontent.com/SecOps-Institute/Tor-IP-Addresses/master/tor-exit-nodes.lst';

    /**
     * A storage array for our parsed tor exit nodes.
     *
     * @var array
     */
    public $exitNodes = array();

    /**
     * A holder for our downloaded files.
     *
     * @var string
     */
    public $download = null;

    /**
     * Get a copy of the current exit node list from the tor site.
     *
     * @return this
     */
    public function get(){

        // Clear 
        $this->download = null;

        // Get File.
        $this->download .= file_get_contents($this->downloadURL);

        // Verify we got a valid response.
        if(!strpos($http_response_header[0], "200")){

            return false;

        }

        // Get File.
        $this->download .= file_get_contents($this->formattedList);

        // Verify we got a valid response.
        if(!strpos($http_response_header[0], "200")){

            return false;

        }
        
        
        // support chaining.
        return $this;

    }

    /**
     * Parse the Downloaded List of Tor Exit nodes, and append to array.
     *
     * @return this
     */
    public function parse(){

        // Explode
        $lines = explode(PHP_EOL, $this->download);

        $this->exitNodes = array();

        // Loop through all lines.
        foreach($lines as $line){

            // Ignore Comments.
            if(substr($line, 0, 1) === "#"){

                continue;

            }

            // Append to array.
            $this->exitNodes[]['ip'] = trim($line);

        }

        return $this;

    }


    /**
     * Get the ASN data for each IP on the list.
     *
     * @return this
     */
    public function get_asn(){

        $ip2asn = new \ip2asn\core;

        foreach($this->exitNodes as $key => $value){

            $this->exitNodes[$key]['asn'] = $ip2asn->get($value['ip'])->ASNs();

        }

        return $this;

    }

    /**
     * Cache the exitnodes data.
     *
     * @param string $url to where you wish to store the cache file.
     * @return this
     */
    public function store(string $url){

        $data = json_encode($this->exitNodes, JSON_PRETTY_PRINT);

        file_put_contents($url, $data);

        return $this;

    }


    /**
     * Retrieve exitnodes cache.
     *
     * @param string $url to the cache location.
     * @return this
     */
    public function load(string $url){

        $data = file_get_contents($url);

        $this->exitNodes = json_decode($data, true);

        return $this;

    }

    /**
     * Search for an Exitnode using an IP.
     *
     * @param string $ip
     * @return this
     */
    public function ipSearch(string $ip){

        $key = array_search($ip, array_column($this->exitNodes, 'ip'));

        if(is_int($key)){

            return $this->exitNodes[$key];

        }

        return $key;

    }


    /**
     * Dump our database. 
     *
     * @return array
     */
    public function dump(){

        return $this->exitNodes;

    }

    /**
     * Import array into exit nodes.
     *
     * @param array $exitNodes
     * @return this
     */
    public function import(array $exitNodes){

        $this->exitNodes = $exitNodes;

        return $this;

    }

}
